# -*- coding: utf-8 -*-
import json
import random

import telegram
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater
from telegram.ext import MessageHandler
from telegram.ext import CommandHandler, RegexHandler
from telegram.ext import Filters

import boto3

from awsconversationhandler import AwsConversationHandler

bot = telegram.Bot('474525635:AAH5m_A5fJS_mQq8yXA05bhhOej-0KcXGAc')
updater = Updater(bot=bot)
dp = updater.dispatcher

# ==easy echo==
# def start(bot, update):
#     update.message.reply_text("Hi!")
#
# def echo(bot, update):
#     update.message.reply_text(update.message.text)
#
# dp.add_handler(CommandHandler("start", start))
# dp.add_handler(MessageHandler(Filters.text, echo))

# https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/conversationbot.py
FOOD, PHOTO, LOCATION, BIO = range(4)

def start(bot, update):
    reply_keyboard = [['Boy', 'Мацарела']]

    update.message.reply_text(
        'Привет это текст'
        'Он не сильно длинный'
        'Send /cancel to stop talking to me.\n\n'
        'Are you a boy or a girl?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return FOOD

def food(bot, update):
    user = update.message.from_user
    # logger.info("Gender of %s: %s" % (user.first_name, update.message.text))
    update.message.reply_text('I see! Please send me a photo of yourself, '
                              'so I know what you look like, or send /skip if you don\'t want to.',
                              reply_markup=ReplyKeyboardRemove())

    return PHOTO

def photo(bot, update):
    user = update.message.from_user
    photo_file = bot.get_file(update.message.photo[-1].file_id)
    photo_file.download('user_photo.jpg')
    # logger.info("Photo of %s: %s" % (user.first_name, 'user_photo.jpg'))
    update.message.reply_text('Gorgeous! Now, send me your location please, '
                              'or send /skip if you don\'t want to.')

    return LOCATION


def skip_photo(bot, update):
    user = update.message.from_user
    # logger.info("User %s did not send a photo." % user.first_name)
    update.message.reply_text('I bet you look great! Now, send me your location please, '
                              'or send /skip.')

    return LOCATION


def location(bot, update):
    user = update.message.from_user
    user_location = update.message.location
    # logger.info("Location of %s: %f / %f"
                # % (user.first_name, user_location.latitude, user_location.longitude))
    update.message.reply_text('Maybe I can visit you sometime! '
                              'At last, tell me something about yourself.')

    return BIO


def skip_location(bot, update):
    user = update.message.from_user
    # logger.info("User %s did not send a location." % user.first_name)
    update.message.reply_text('You seem a bit paranoid! '
                              'At last, tell me something about yourself.')

    return BIO


def bio(bot, update):
    user = update.message.from_user
    # logger.info("Bio of %s: %s" % (user.first_name, update.message.text))
    update.message.reply_text('Thank you! I hope we can talk again some day.')

    return AwsConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    #logger.info("User %s canceled the conversation." % user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return AwsConversationHandler.END

dynamodb = boto3.resource("dynamodb")
table = dynamodb.Table("sergiev-posad-help-bot-dev")

conv_handler = AwsConversationHandler(
    entry_point=CommandHandler('start', start),

    states={
        FOOD: [RegexHandler(u'^(Boy|Мацарела)$', food)],

        PHOTO: [MessageHandler(Filters.photo, photo),
                CommandHandler('skip', skip_photo)],

        LOCATION: [MessageHandler(Filters.location, location),
                   CommandHandler('skip', skip_location)],

        BIO: [MessageHandler(Filters.text, bio)]
    },

    fallback=CommandHandler('cancel', cancel),
    dynamo_table = table
)

dp.add_handler(conv_handler)

def entry(event, context):
    body = json.loads(event["body"])
    update = telegram.Update.de_json(body, bot)
    dp.process_update(update)

    response = {
        "statusCode": 200,
        "body": "OK"
    }

    return response

if __name__ == "__main__":
    print(bot.set_webhook("https://5fm7g39e0k.execute-api.us-east-1.amazonaws.com/dev/0KcXGAc"))

# -*- coding: utf-8 -*-

from telegram import Update
from telegram.ext import (Handler, CallbackQueryHandler, InlineQueryHandler,
                          ChosenInlineResultHandler)
from telegram.utils.promise import Promise

from botocore.exceptions import ClientError

class AwsConversationHandler(Handler):
    END = -1
    def __init__(self,
                 entry_point,
                 states,
                 fallback,
                 dynamo_table):
        self.entry_point = entry_point
        self.states = states
        self.fallback = fallback
        self.dynamo_table = dynamo_table

        self.candidate_to_handle = None


    def get_state(self, update):
        user_id = update.effective_user.id

        key_item = {"id": str(user_id)}
        try:
            response = self.dynamo_table.get_item(Key=key_item)
        except ClientError as e:
            print("error :(")
            print(e.response['Error']['Message'])
            return None
        else:
            item = response.get("Item", None)
            if not item:
                return None
            return int(item["chat_state"])

    def set_state(self, state, update):
        user_id = update.effective_user.id

        item = {"id":str(user_id), "chat_state":str(state)}

        response = self.dynamo_table.put_item(Item=item)


    def check_update(self, update):
        # Ignore messages in channels
        # if (not isinstance(update, Update) or update.channel_post or self.per_chat
        #         and (update.inline_query or update.chosen_inline_result) or self.per_message
        #         and not update.callback_query or update.callback_query and self.per_chat
        #         and not update.callback_query.message):
        #     return False

        current_state = self.get_state(update)

        print("update is: {}".format(update))
        print("current state: {}".format(current_state))
        print("states are: {}".format(self.states))

        if current_state == None or current_state == -1:
            if self.entry_point.check_update(update):
                self.candidate_to_handle = self.entry_point
        else:
            for candidate in self.states[current_state]:
                print("candidate is: {}".format(candidate))
                print("candidate check_update: {}".format(candidate.check_update(update)))
                if candidate.check_update(update):
                    self.candidate_to_handle = candidate

        print("candidate: {}".format(self.candidate_to_handle))
        return self.candidate_to_handle != None

    def handle_update(self, update, dispatcher):
        """Send the update to the callback for the current state and Handler

        Args:
            update (:class:`telegram.Update`): Incoming telegram update.
            dispatcher (:class:`telegram.ext.Dispatcher`): Dispatcher that originated the Update.

        """
        new_state = self.candidate_to_handle.handle_update(update, dispatcher)
        self.set_state(new_state, update)
